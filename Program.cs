﻿using System;

namespace week5_ex24_b
{
    class Program
    {
        static void Main(string[] args)
        {
            Method("Jesus");
            Method("John");
            Method("Jacob");
            Method("Jarred");
            Method("James");
            
            Console.WriteLine("Type another name to display");
            Method(Console.ReadLine());
        }
        static void Method(string name)
        {
            Console.WriteLine(name);
        }
    }
}
